package com.muyangren.controller;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.muyangren.entity.EsTransMap;
import com.muyangren.entity.TransEsContent;
import com.muyangren.entity.TransEsSearch;
import com.muyangren.service.ElasticService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @author: muyangren
 * @Date: 2023/1/18
 * @Description: com.muyangren.controller
 * @Version: 1.0
 */
@RestController
@RequestMapping("elasticSearch")
@Api(value = "ES测试接口", tags = "ES测试接口")
public class ElasticController {

    @Resource
    private ElasticService elasticService;

    @GetMapping("/syncAllEsTrans")
    @ApiOperation(value = "初始化数据", notes = "初始化数据")
    @ApiOperationSupport(order = 1)
    public Boolean syncAllEsTrans() {
        return elasticService.initEsData();
    }

    @GetMapping("/addEsTrans")
    @ApiOperation(value = " 添加数据", notes = "添加数据")
    @ApiOperationSupport(order = 2)
    public Boolean addEsTrans(TransEsContent transEsContent) {
        return elasticService.addEsTrans(transEsContent);
    }

    @PostMapping("/updateEsTrans")
    @ApiOperation(value = " 修改数据", notes = "修改数据")
    @ApiOperationSupport(order = 3)
    public Boolean updateEsTrans(TransEsContent transEsContent) {
        return elasticService.updateEsTrans(transEsContent);
    }

    @GetMapping("/delEsTrans")
    @ApiOperation(value = "删除数据", notes = "删除数据")
    @ApiOperationSupport(order = 4)
    public Boolean delEsTrans(Integer id) {
        return elasticService.delEsTrans(id);
    }

    @GetMapping("/integrationQuery")
    @ApiOperation(value = "整合模糊查询，分页", notes = "参数为搜索内容")
    @ApiOperationSupport(order = 12)
    public EsTransMap integrationQuery(TransEsSearch transEsSearch) {
        return elasticService.selectEsTransPage(transEsSearch);
    }
}
