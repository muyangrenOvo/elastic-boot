package com.muyangren.service;

import com.muyangren.entity.EsTransMap;
import com.muyangren.entity.TransEsContent;
import com.muyangren.entity.TransEsSearch;

import java.io.IOException;

/**
 * @author: muyangren
 * @Date: 2023/1/29
 * @Description: com.muyangren.service
 * @Version: 1.0
 */
public interface ElasticService {

    /**
     * 初始化数据
     * @return
     */
    Boolean initEsData();

    /**
     * 添加数据
     * @return
     */
    Boolean addEsTrans(TransEsContent transEsContent);

    /**
     * 删除数据
     * @param id
     * @return
     */
    Boolean delEsTrans(Integer id);

    /**
     * 更新数据
     * @param transEsContent
     * @return
     */
    Boolean updateEsTrans(TransEsContent transEsContent);

    /**
     * 分页
     * @param transEsSearch
     * @return
     */
    EsTransMap selectEsTransPage(TransEsSearch transEsSearch);
}
