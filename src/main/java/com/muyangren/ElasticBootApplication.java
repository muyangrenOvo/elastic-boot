package com.muyangren;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author guangsheng
 */
@SpringBootApplication
public class ElasticBootApplication {

    public static void main(String[] args) {
        SpringApplication.run(ElasticBootApplication.class, args);
    }

}
