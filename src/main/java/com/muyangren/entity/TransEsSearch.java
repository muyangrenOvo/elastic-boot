package com.muyangren.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author: muyangren
 * @Date: 2023/1/31
 * @Description: 条件搜索
 * @Version: 1.0
 */
@Data
@ApiModel(value = "TransEsContentDTO", description = "查询参数")
public class TransEsSearch implements Serializable {
    private static final long serialVersionUID = 1L;
    @ApiModelProperty("第几页")
    private int pageNo;
    @ApiModelProperty("每页大小")
    private int pageSize;
    @ApiModelProperty("类型")
    private Integer type;
    @ApiModelProperty("搜索关键字")
    private String keyword;
    @ApiModelProperty("开始时间（yyyy-MM-dd）")
    private String beginDate;
    @ApiModelProperty("结束时间（yyyy-MM-dd）")
    private String endDate;
}
