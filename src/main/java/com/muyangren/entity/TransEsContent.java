package com.muyangren.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author: muyangren
 * @Date: 2023/1/18
 * @Description: com.muyangren.entity
 * @Version: 1.0
 */
@Data
public class TransEsContent implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键id")
    private Integer id;

    @ApiModelProperty("类型 1-普通 2-高级 3-特级 4-最高级")
    private Integer type;

    @ApiModelProperty("标题")
    private String title;

    @ApiModelProperty("原内容")
    private String content;

    @ApiModelProperty("带高亮内容")
    private String highlightContent;

    @ApiModelProperty("创建时间")
    private String createTime;

    @ApiModelProperty("创建时间(时间戳)")
    private Long createTimeLong;

}
