package com.muyangren.entity;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

/**
 * @author guangsheng
 * @Auther: muyangren
 * @Date: 2022/7/15
 * @Description: ES转写集合
 * @Version: 1.0
 */
@Data
@EqualsAndHashCode()
@ApiModel(value = "ES集合", description = "ES集合")
public class EsTransMap implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long total;
    private Long queryTime;
    private List<TransEsContent> records;
}
