package com.muyangren.constants;

/**
 * @author: muyangren
 * @Date: 2023/1/31
 * @Description: com.muyangren.constants
 * @Version: 1.0
 */
public interface EsConstant {

    String CONTENT = "content";
    String CREATE_TIME_LONG = "createTimeLong";
    String MIN_SEARCH_DATE = "2000-03-17 00:00:00";
    String TYPE = "type";
}
