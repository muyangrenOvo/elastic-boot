package com.muyangren;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class ElasticBootApplicationTests {


    @Value("${elasticsearch.analyzer}")
    private String analyzer;
    @Value("${elasticsearch.index}")
    private String index;
    @Autowired
    private ElasticsearchClient esClient;

    @Test
    void contextLoads() {
    }

}
