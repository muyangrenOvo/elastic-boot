# springboot整合elasticsearch+hanlp分词器（7.16.2）

#### 介绍

1. 目前官网hanlp只支持到es7.10及之前的版本，如果你在项目中是使用最新版本的elastic，并且业务需求是需要用到hanlp分词器的，那么这里提供一个测试用例供大家参考

#### 安装教程
[docker部署elasticsearch7.16.2 + hanlp分词器](https://blog.csdn.net/m0_51706962/article/details/128938810)

#### 使用说明

1.  配置yml
![输入图片说明](1.jpg)

2.  启动
![输入图片说明](1.1.jpg)

3.  输入网址  http://localhost:8899/doc.html#/home
![输入图片说明](2.jpg)
4. 初始化数据
![输入图片说明](3.jpg)

5. 然后调用分页接口测试
![输入图片说明](4.jpg)
演示到此结束，如果觉得对您有帮助，可以帮忙点个star哈